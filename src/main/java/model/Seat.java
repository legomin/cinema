package model;

import org.springframework.boot.orm.jpa.EntityScan;

import java.io.Serializable;

/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author pankaj
 *
 */
public class Seat implements Serializable{

	private int row, colum;
	
	private boolean reserved;

	public Seat() {
	}

	public Seat(int row, int colum) {
		this.row = row;
		this.colum = colum;
		this.reserved = false;
	}

	public int getRow() {
		return row;
	}

	public int getColum() {
		return colum;
	}

	public boolean isReserved() {
		return reserved;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public void setColum(int colum) {
		this.colum = colum;
	}

	public void setReserved(boolean reserved) {
		this.reserved = reserved;
	}

	@Override
	public String toString() {
		return "Seat{" +
				"row=" + row +
				", colum=" + colum +
				", reserved=" + reserved +
				'}';
	}
}
