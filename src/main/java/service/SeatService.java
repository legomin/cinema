package service;


import dao.SeatDAO;
import model.Seat;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class SeatService {
	
	private SeatDAO seatDAO = new SeatDAO();
	public void setSeatDAO() {}

	public List<List<Seat>> getCinema() {
		return seatDAO.getCinema();
	}

	public void saveCinema(List<List<Seat>> cinema){
		seatDAO.saveCinema(cinema);
	}

}
