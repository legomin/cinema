package controller;


import model.Seat;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import service.SeatService;

import java.util.List;

@RestController

public class Controller {


    SeatService seatService = new SeatService();

    @RequestMapping(value = "/cinema", method = RequestMethod.GET)
     public List<List<Seat>> seats() {

        return seatService.getCinema();
    }

    @RequestMapping(value = "/cinema", method = RequestMethod.POST)
    public String saveSettings(@RequestBody List<List<Seat>> cinema, Model model ){
        seatService.saveCinema(cinema);
        return "susessful!!";
    }

}