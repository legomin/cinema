package dao;

import model.Seat;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SeatDAO {

	private static ObjectInputStream objectInputStream;
	private static ObjectOutputStream objectOutputStream;
	private static List<List<Seat>> cinema;

	static {

		try {
			objectInputStream = new ObjectInputStream(new FileInputStream("data.txt"));
			cinema = (List<List<Seat>>) objectInputStream.readObject();
		}catch (IOException e) {
			try {
				cinema = parseCinema();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			objectOutputStream = new ObjectOutputStream(new FileOutputStream("data.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static List<List<Seat>> parseCinema() throws IOException {
		List<List<Seat>> result = new ArrayList<>();
		BufferedReader fileReader = new BufferedReader(new FileReader("cinema.txt"));
		while (fileReader.ready()){
			String[] s = fileReader.readLine().split(" ");
			int row = Integer.parseInt(s[0].trim());
			int colum = Integer.parseInt(s[1].trim());
			try{
				result.get(row-1).add(new Seat(row,colum));
			}
			catch (IndexOutOfBoundsException e){
				result.add(new ArrayList<>());
				result.get(row-1).add(new Seat(row,colum));
			}
		}
		fileReader.close();

		return result;
	}

	public List<List<Seat>> getCinema() {
		return cinema;
	}

    public void saveCinema(List<List<Seat>> cinema){
        setCinema(cinema);
		try {
            objectOutputStream.writeObject(cinema);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public static void setCinema(List<List<Seat>> cinema) {
		SeatDAO.cinema = cinema;
	}
}
