/**
 * Created by Виталий on 21.12.2015.
 */
function Cinema($scope,$http){
    $http.get('/cinema').
    success(function(data) {
        $scope.seats = data;
    });

    $scope.reserve = function(column) {
        if (column.reserved==0)
            column.reserved = 2;
        else if (column.reserved==2)
            column.reserved = 0;
    };

     $scope.saveSeats = function () {
         if ($scope.name == '' || $scope.email == ''){
             alert("name & email need to be filled !");
             return;
         }

         var i, j ,countReservedSeats = 0;
          for (i = $scope.seats.length-2; i < $scope.seats.length; i++) {
              countReservedSeats = 0;
              for (j = 0; j < $scope.seats[i].length; j++){
                if ($scope.seats[i][j].reserved == 2){
                    countReservedSeats++;
                }
                else{
                    if (countReservedSeats % 2 !=0){
                        alert("In 2 last rows might be only pair's reserve!");
                        return;
                    }
                    countReservedSeats = 0;
                }
             }
              if (countReservedSeats % 2 !=0){
                  alert("In 2 last rows might be only pair's reserve!");
                  return;
              }
          }
         $http.post('/cinema', angular.toJson($scope.seats)).success(function(response){
            alert(response);
             $http.get('/cinema').
             success(function(data) {
                 $scope.seats = data;
             });

        });


     }
}

